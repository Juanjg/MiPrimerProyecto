<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="mipk.beanDB"%>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Creative - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="css/animate.min.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/creative.css" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    #consultas{
        border-style: outset;
        width:370px;
    	position:relative;
    	left:370px;
    	border:solid;  	
    	bottom:250px;
    }
    th, td{
	    border-style: outset;
	    
	    padding: 12px;
    	position:relative;
    	border:solid 1px gray;     	
    }
    
    th{
    	text-align:center;
    	color:white;
    	background-color:darkorange;
    }
    #vacio{
		color:red;
	}
    #boton{
    	position:relative;
    	left:30px;
    }
    #var,#var1,#var2,#var3,#var4{
	    position:relative;
	    margin:auto;
    	width:200px;
    	color: gray;
    	left:18px;
    }
    #var1{
    	left:35px;
    }
    #var2{
    	left:6px;
    }
    #var3{
    	left:40px;
    }
    #var4{
    	left:28px;
    }  
    label{
    	position:relative;
    	margin:auto;
    	text-align:left;
    }
    #cont{
    	position :relative;	
    	right:450px;
    }
    #idsal{
    	position:relative;
    	right:5px;
    	bottom:1px;
    }
    #titulo,#vacio,#filas,#raya{
    	position:relative;
    	left: 120px;
    }
    
    </style>

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="index2.jsp" class="btn btn-default btn-xl">Volver</a>
                <a href="#portfolio" class="btn btn-default btn-xl">Bajar</a>
                <a href="#services" class="btn btn-default btn-xl">Subir</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">                  
                    <li>
                        <a class="btn btn-default btn-xl"></a>
                    </li>
                    <li>
                        <a class="btn btn-default btn-xl"></a>
                    </li>                 
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <br>
   <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 id="titulo" class="section-heading">Actualizar Registros de Salas</h2>
                    <hr id="raya" class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                   	      <%!String valorobtenido = ""; %>
                   	      
                   	      <% 
							try
							{
								valorobtenido = request.getParameter("valor").toString();
							}
							catch (Exception e)
							{}
                   	      	
							beanDB basededatos = new beanDB();	
		            		String query3 = "select sal_codigo,sal_descripcion,sal_admin,sal_nombre_admin,sal_email_admin, sal_telefono_admin, sal_pass from salas";
							query3 += " where sal_codigo = "+valorobtenido+";"; 
							String [][] tablares2 = basededatos.resConsultaSelectA3(query3);
							%> 
							
							<form name="introgrupo" action="index3.jsp" method="GET">
							<div id="cont">								
							<label>Codigo Sala: </label>
								
							<input id ="var" type="text" name="valor1" value="<%=tablares2[0][0]%>">
							<br><br>
							
							<label>Descripcion: </label>	
							<input id ="var" type="text" name="valor2" value="<%=tablares2[0][1]%>">
							<br><br>
							
							<label>Alias Admin: </label>	
							<input id ="var" type="text" name="valor3" value="<%=tablares2[0][2]%>">
							<br><br>
							
							<label>Nombre Admin: </label>	
							<input id ="var2" type="text" name="valor4" value="<%=tablares2[0][3]%>">
							<br><br>
							
							<label>Email: </label>	
							<input id ="var3" type="text" name="valor5" value="<%=tablares2[0][4]%>">
							<br><br>
							
							<label>Telefono: </label>	
							<input id ="var4" type="text" name="valor6" value="<%=tablares2[0][5]%>">
							<br><br>
							
							<label>Contraseña: </label>	
							<input id ="var" type="password" name="valor7" value="<%=tablares2[0][6]%>">
							<br><br>							
							<input id="boton" type="submit" value="Actualizar">
							</div>							
							</form>
																					
							<%	
							
							String[] valor = new String[7];
							try
							{
							valor[0]=request.getParameter("valor1").toString();							
							}
							catch (Exception e)
							{}
							
							
							if(valor[0] != null){
							
							try{							
															
								valor[1]=request.getParameter("valor2").toString();
								valor[2]=request.getParameter("valor3").toString();
								valor[3]=request.getParameter("valor4").toString();
								valor[4]=request.getParameter("valor5").toString();
								valor[5]=request.getParameter("valor6").toString();
								valor[6]=request.getParameter("valor7").toString();
								
							}catch(Exception e){}
							}
							int cont = 0;
							for(int i=0; i<valor.length;i++) {
								if(valor[i]!= null)
									cont++;
							}	
							
							if(cont != 0){
							String query="UPDATE salas SET ";
							
							query +="sal_codigo = '"+valor[0];
							
							query +="' , sal_descripcion = '"+valor[1];
							
							query +="' , sal_admin = '"+valor[2];
							
							query +="' , sal_nombre_admin = '"+valor[3];
							
							query +="' , sal_email_admin = '"+valor[4];
							
							query +="' , sal_telefono_admin = '"+valor[5];
							
							query +="' , sal_pass = '"+valor[6]; 
							
							query += "' where sal_codigo = "+ valorobtenido +";";
							if(query != " "){
							basededatos.update(query);
							}		
							String query2 = "select sal_codigo,sal_descripcion,sal_admin,sal_nombre_admin,sal_email_admin, sal_telefono_admin, sal_pass from salas";
							query2 += " where sal_codigo = "+valorobtenido+";"; 
							String [][] tablares = basededatos.resConsultaSelectA3(query2);							
							%>
							<table id= "consultas">
							<%
							if(tablares != null){
							%>					
							   <tr>
								<th>ID Sala </th>
								<th>Codigo Sala </th>
								<th>Descripcion</th>
								<th>Administrador</th>
								<th>Email Admin</th>
								<th>Telefono</th>
								<th>Contraseña</th>
								</tr>
							
							<% for (int i=0; i<tablares.length;i++) {
								%><tr> <%
								for (int j=0; j<tablares[i].length;j++) {
								%> <td> <%=tablares[i][j] %> </td> 
								<% 
								}
								%> </tr> <%
							}
							}else{
								%><h3 id="vacio">Conjunto Vacío</h3><% 
							}
							%>
						</table>    
						
						  <%} %>
                    </div>
                </div>
            </div>
        </div>
       
    </section>

<section class="no-padding" id="portfolio">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-lg-4 col-sm-6">
                    <a href="index1.jsp" class="portfolio-box">
                        <img src="img/portfolio/1.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    NºAlumno - Curso
                                </div>
                                <div class="project-name">
                                    <h2>Primera Query</h2>
                                    <!-- Consultas -->
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="index2.jsp" class="portfolio-box">
                        <img src="img/portfolio/2.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    NºFalta - Grupo
                                </div>
                                <div class="project-name">
                                    <h2>Segunda Query</h2>
                                    <!-- Datos Personales -->
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="index4.jsp" class="portfolio-box">
                        <img src="img/portfolio/3.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Proximamente
                                </div>
                                <div class="project-name">
                                    <h2>Tercera Query</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                               
            </div>
        </div>
    </section>   

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/wow.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/creative.js"></script>

</body>

</html>
