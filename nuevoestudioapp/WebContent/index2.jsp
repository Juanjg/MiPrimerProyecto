<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="mipk.beanDB"%>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Creative - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="css/animate.min.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/creative.css" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
   table{
        border-style: outset;
        width:1100px;
    	position:relative;  	
    	border:solid;  	   	
    	right:-40px;
    }   
   
    th,td{
	    border-style: outset;
	    
	    padding: 12px;
    	position:relative;
    	border:solid 1px gray;     	
    }
    td{
    	height: 25px;
    }
    
    th{
    	text-align:center;
    	color:white;
    	background-color:darkorange;
    }
    #vacio{
		color:red;
	}
    #boton{
    	position:relative;
    	left: 10px;    	
    }
    #var{
	    position:relative;
	    margin:auto;
    	width:250px;
    	color:gray;
    	right:85px;
    	top:13px;
    }    
    label{
    	position:relative;
    	margin:auto;
    	text-align:left;
    	top: 20px;
    	right:160px;
    }
    #cont{
    	position :relative;	
    	right:390px;
    }
    #idsal{
    	position:relative;
    	right:5px;
    	bottom:1px;
    }
    #titulo,#vacio,#filas,#raya{
    	position:relative;
    	left: 120px;
    }
    select,#col{
    	position:relative;
    	right: 480px;
    }
    
    #elige{
    	position:relative;
    	right:390px;
    	top:30px;
    }
    </style>

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="index.jsp" class="btn btn-default btn-xl">Inicio</a>
                <a href="#portfolio" class="btn btn-default btn-xl">Bajar</a>
                <a href="#services" class="btn btn-default btn-xl">Subir</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">                  
                    <li>
                        <a class="btn btn-default btn-xl"></a>
                    </li>
                    <li>
                        <a class="btn btn-default btn-xl"></a>
                    </li>                 
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <br>
   <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Listado de Salas a Modificar</h2>
                    <hr class="primary">
                    <h3 id="elige">Elije la sala a modificar:</h3>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">                   	      
														
								<br> <br> <br>
								<%
									beanDB basededatos = new beanDB();
									String query = "select sal_codigo from salas;";
									String[][] tablares = basededatos.resConsultaSelectA3(query);																										
								
									String query2 = "select sal_codigo,sal_descripcion,sal_admin,sal_nombre_admin,sal_email_admin, sal_telefono_admin, sal_pass from salas;";								
									String[][] tablares2 = basededatos.resConsultaSelectA3(query2);
								%>		
							
									
							<table>
							<%
							if(tablares2 != null){
							%> 							
							   <tr>
							    <th>Check</th>								
								<th>Codigo </th>
								<th>Descripcion</th>
								<th>Alias</th>
								<th>Administrador</th>
								<th>Email </th>
								<th>Telefono</th>
								<th>Contraseña</th>
								</tr>	
							
													
							<% 
							
							int cont = 1;
							
							for (int i=0; i<tablares2.length;i++) {
								%><tr>
								<%switch(cont){ 
								
									case 1:%>
								<td><input type="button" onClick=" window.location.href='index3.jsp?valor=<%=tablares[0][0]%>'" value="Editar"></td>
								 <%break;
									case 2:%>
									<td><input type="button" onClick=" window.location.href='index3.jsp?valor=<%=tablares[1][0]%>'" value="Editar"></td>
									 <%break;
									case 3:%>
									<td><input type="button" onClick=" window.location.href='index3.jsp?valor=<%=tablares[2][0]%>'" value="Editar"></td>
									 <%break;
									case 4:%>
									<td><input type="button" onClick=" window.location.href='index3.jsp?valor=<%=tablares[3][0]%>'" value="Editar"></td>
									 <%break;
									case 5:%>
									<td><input type="button" onClick=" window.location.href='index3.jsp?valor=<%=tablares[4][0]%>'" value="Editar"></td>
									 <%break;
									case 6:%>
									<td><input type="button" onClick=" window.location.href='index3.jsp?valor=<%=tablares[5][0]%>'" value="Editar"></td>
									 <%break;
									case 7:%>
									<td><input type="button" onClick=" window.location.href='index3.jsp?valor=<%=tablares[6][0]%>'" value="Editar"></td>
									 <%break;
									case 8:%>
									<td><input type="button" onClick=" window.location.href='index3.jsp?valor=<%=tablares[7][0]%>'" value="Editar"></td>
									 <%break;
									case 9:%>
									<td><input type="button" onClick=" window.location.href='index3.jsp?valor=<%=tablares[8][0]%>'" value="Editar"></td>
									 <%break;
									case 10:%>
									<td><input type="button" onClick=" window.location.href='index3.jsp?valor=<%=tablares[9][0]%>'" value="Editar"></td>
									 <%break;
									case 11:%>
									<td><input type="button" onClick=" window.location.href='index3.jsp?valor=<%=tablares[10][0]%>'" value="Editar"></td>
									 <%break;
									case 12:%>
									<td><input type="button" onClick=" window.location.href='index3.jsp?valor=<%=tablares[11][0]%>'" value="Editar"></td>
									 <%break;
								}
								for (int j=0; j<tablares2[i].length;j++) {
								%> <td> <%=tablares2[i][j] %> </td> 
								<% 
								}
								%> </tr> <%
								cont++;
							}
							}else{
								%><h3 id="vacio">Conjunto Vacío</h3>
								<%}
							
								
							%>
						</table>							
																	    						                     
                    </div>
                </div>
            </div>
        </div>
       
    </section>

<section class="no-padding" id="portfolio">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-lg-4 col-sm-6">
                    <a href="index1.jsp" class="portfolio-box">
                        <img src="img/portfolio/1.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    NºAlumno - Curso
                                </div>
                                <div class="project-name">
                                    <h2>Primera Query</h2>
                                    <!-- Consultas -->
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="index2.jsp" class="portfolio-box">
                        <img src="img/portfolio/2.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    NºFalta - Grupo
                                </div>
                                <div class="project-name">
                                    <h2>Segunda Query</h2>
                                    <!-- Datos Personales -->
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="index4.jsp" class="portfolio-box">
                        <img src="img/portfolio/3.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Proximamente
                                </div>
                                <div class="project-name">
                                    <h2>Tercera Query</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                              
            </div>
        </div>
    </section>   

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/wow.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/creative.js"></script>

</body>

</html>